#!/bin/bash

YELLOW='\033[0;33m'
NC='\033[0m' # No Color

if [[ $(git status --porcelain) ]]; then
    echo -e "${YELLOW}You have uncommitted changes in your repo. You must commit these before releasing.${NC}" >&2
    exit 1
fi

npx standard-version --skip.commit --skip.tag

PACKAGE_VERSION=$(node -p -e "require('./package.json').version")

npm version $PACKAGE_VERSION --no-git-tag-version --allow-same-version

git add .
git commit -m "chore(release): ${PACKAGE_VERSION}"
git tag -a v${PACKAGE_VERSION} -m "chore(release): ${PACKAGE_VERSION}"

echo "Run \"git push --follow-tags\" and GitLab will publish the packages"
