# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.0.1](https://gitlab.com/dotdevv/packages/sanity-copy-document-action/compare/v1.0.0...v1.0.1) (2021-09-26)

## [1.0.0](https://gitlab.com/dotdevv/packages/sanity-copy-document-action/compare/v0.1.8...v1.0.0) (2021-09-26)

### Features

- document copier endpoint ([e36724c](https://gitlab.com/dotdevv/packages/sanity-copy-document-action/commit/e36724cc73391ba47c9342a36a578f0435560bc5))

### [0.1.8](https://gitlab.com/dotdevv/packages/sanity-copy-document-action/compare/v0.1.7...v0.1.8) (2021-09-19)

### [0.1.7](https://gitlab.com/dotdevv/packages/sanity-copy-document-action/compare/v0.1.6...v0.1.7) (2021-09-19)

### [0.1.6](https://gitlab.com/dotdevv/packages/sanity-copy-document-action/compare/v0.1.5...v0.1.6) (2021-09-19)

### Features

- add view target document button ([d0b3a22](https://gitlab.com/dotdevv/packages/sanity-copy-document-action/commit/d0b3a226d3e4126448401abed98d3c90eeb7eb27))
- style components using @sanity/ui ([fcb0665](https://gitlab.com/dotdevv/packages/sanity-copy-document-action/commit/fcb0665f16ddee3fa0c1030f55a505d004a6fa2e))

### Bug Fixes

- unknown error type ([321b0ef](https://gitlab.com/dotdevv/packages/sanity-copy-document-action/commit/321b0ef4925642c410c2bd188e9372c9dee1d6ee))
- use ordinary CopyIcon from @sanity/icons ([4c23285](https://gitlab.com/dotdevv/packages/sanity-copy-document-action/commit/4c232859cac445b5e9b4c9b042752674a74c08ba))

### [0.1.5](https://gitlab.com/dotdevv/packages/sanity-copy-document-action/compare/v0.1.4...v0.1.5) (2021-03-24)

### [0.1.4](https://gitlab.com/dotdevv/packages/sanity-copy-document-action/compare/v0.1.3...v0.1.4) (2021-03-24)

### Bug Fixes

- ignore drafts, disable action when not published or in draft ([725c996](https://gitlab.com/dotdevv/packages/sanity-copy-document-action/commit/725c99647a834561b314b9d6f38446e4eb7484bd))

### [0.1.3](https://gitlab.com/dotdevv/packages/sanity-copy-document-action/compare/v0.1.2...v0.1.3) (2021-03-24)

### Bug Fixes

- correctly resolve current dataset when using sanity spaces ([06dccf9](https://gitlab.com/dotdevv/packages/sanity-copy-document-action/commit/06dccf971032434abb363261075a58da6e3fcfd7))
- draft takes precedence over published when resolving current document ([6576d78](https://gitlab.com/dotdevv/packages/sanity-copy-document-action/commit/6576d78484944bde086c4df650315ff53bf4daee))

### [0.1.2](https://gitlab.com/dotdevv/packages/sanity-copy-document-action/compare/v0.1.1...v0.1.2) (2021-03-24)

### Bug Fixes

- path and dist location for types ([54ec0bb](https://gitlab.com/dotdevv/packages/sanity-copy-document-action/commit/54ec0bb00d5ac035244b3afa64154f4fd340afc3))

### 0.1.1 (2021-03-24)
