import type { CopyTarget } from "./types";

// get current dataset using URL if necessary
// the configured dataset for this desk should be passed into this helper
// if the desk is using spaces, the "default" dataset does not change, so we try to detect it from the URL
export const resolveCurrentDataset = (
  defaultDataset: string,
  copyTargets: CopyTarget[]
): string => {
  const pathParts = window.location.pathname.split("/");

  if (
    pathParts[1] &&
    copyTargets.map((copyTarget) => copyTarget.dataset).includes(pathParts[1])
  ) {
    return pathParts[1];
  }

  return defaultDataset;
};
