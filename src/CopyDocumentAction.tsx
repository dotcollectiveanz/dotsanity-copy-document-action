import React from "react";
import { CopyIcon } from "@sanity/icons";

import { Dialog } from "./components/Dialog";

import type {
  DocumentActionComponent,
  BuildCopyDocumentActionProps,
} from "./types";

export const buildCopyDocumentAction = (
  props: BuildCopyDocumentActionProps
): DocumentActionComponent => {
  return ({ draft, published, onComplete }) => {
    const [dialogOpen, setDialogOpen] = React.useState(false);

    return {
      label: "Copy to…",
      icon: CopyIcon,
      onHandle: () => {
        setDialogOpen(true);
      },
      disabled: !published || !!draft,
      dialog:
        !!published && !draft && dialogOpen
          ? {
              type: "modal",
              title: "Copy to…",
              onClose: onComplete,
              content: <Dialog {...props} document={published} />,
            }
          : undefined,
    };
  };
};
