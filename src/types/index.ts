import type { SanityClient } from "@sanity/client";

import type { Document } from "./sanityTypes";

export * from "./sanityTypes";

export type BuildCopyDocumentActionProps = {
  dataset: string;
  projectId: string;
  copyTargets: CopyTarget[];
  reactifySanity: {
    shopName: string;
    endpointPrefix: string;
  };
};

export type CopyTarget = {
  title: string;
  dataset: string;
};

export type DialogProps = BuildCopyDocumentActionProps & {
  document: Document;
};

export type DialogRowProps = DialogProps & {
  copyTarget: CopyTarget;
  baseClient: SanityClient;
};

export type CopyStatus = "checking" | "idle" | "copying";

export interface DocumentCopierRequest {
  source: {
    id: string;
    dataset: string;
  };
  target: {
    dataset: string;
  };
  options?: {
    assets?: DocumentCopierRequestAssetsOption;
    references?: DocumentCopierRequestReferencesOption;
  };
}

export enum DocumentCopierRequestAssetsOption {
  Copy = "copy",
  Ignore = "ignore",
}

export enum DocumentCopierRequestReferencesOption {
  Resolve = "resolve",
  Ignore = "ignore",
  Retain = "retain",
}

export interface DocumentCopierTask {
  taskId: string;
  statusMessage: string;
  status: DocumentCopierTaskStatus;
  copierRequest: DocumentCopierRequest;
}

export enum DocumentCopierTaskStatus {
  Pending = "pending",
  Running = "running",
  Failed = "failed",
  Complete = "complete",
}
