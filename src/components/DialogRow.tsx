import React from "react";
import axios from "axios";
import { useInterval } from "ahooks";
import { EyeOpenIcon } from "@sanity/icons";
import sanityClient, { SanityDocument } from "@sanity/client";

import {
  Box,
  Card,
  Flex,
  Text,
  Stack,
  Button,
  Inline,
  Heading,
  Spinner,
} from "@sanity/ui";

import type {
  CopyStatus,
  DialogRowProps,
  DocumentCopierTask,
  DocumentCopierRequest,
} from "../types";

export const DialogRow: React.FC<DialogRowProps> = (props) => {
  const { projectId, dataset, copyTarget, document, reactifySanity } = props;

  const [targetDocumentExists, setTargetDocumentExists] =
    React.useState<boolean>();

  const [status, setStatus] = React.useState<CopyStatus>("checking");
  const [errorMessage, setErrorMessage] = React.useState<string | undefined>();

  const targetClient = React.useMemo(
    () =>
      sanityClient({
        projectId,
        useCdn: false,
        withCredentials: true,
        dataset: copyTarget.dataset,
      }),
    [copyTarget, projectId]
  );

  // check if target document exists
  React.useEffect(() => {
    targetClient.getDocument(document._id).then((targetDocument) => {
      setStatus("idle");
      setTargetDocumentExists(!!targetDocument);
    });
  }, [document, targetClient]);

  const [documentCopierTask, setDocumentCopierTask] =
    React.useState<DocumentCopierTask>();

  // change main status back to idle when copy task complete
  React.useEffect(() => {
    if (
      documentCopierTask &&
      ["failed", "complete"].includes(documentCopierTask.status)
    ) {
      setStatus("idle");
      if ("complete" === documentCopierTask.status) {
        setTargetDocumentExists(true);
      }
    }
  }, [documentCopierTask]);

  const [checkTaskInterval, setCheckTaskInterval] = React.useState<number>();

  // recheck task when interval is set
  useInterval(async () => {
    if (!documentCopierTask) return;

    try {
      const { data } = await axios.get(
        `${reactifySanity.endpointPrefix}documentCopierRead`,
        {
          params: {
            shop: reactifySanity.shopName,
            taskId: documentCopierTask.taskId,
          },
        }
      );

      const newDocumentCopierTask: DocumentCopierTask = data?.task;

      if (!newDocumentCopierTask) {
        throw new Error(`document copier task could not be fetched`);
      }

      setDocumentCopierTask(newDocumentCopierTask);
    } catch (e) {
      setStatus("idle");
      setCheckTaskInterval(undefined);
      setErrorMessage((e as Error).message);
    }
  }, checkTaskInterval);

  const copyDocument = React.useCallback(async () => {
    if ("idle" !== status) return;

    setStatus("copying");
    setErrorMessage(undefined);

    try {
      const documentCopierRequest: DocumentCopierRequest = {
        source: {
          dataset,
          id: document._id,
        },
        target: {
          dataset: copyTarget.dataset,
        },
      };

      const { data } = await axios.post(
        `${reactifySanity.endpointPrefix}documentCopierCreate`,
        documentCopierRequest,
        { params: { shop: reactifySanity.shopName } }
      );

      const documentCopierTask: DocumentCopierTask = data?.task;

      if (!documentCopierTask) {
        throw new Error(`document copier task could not be created`);
      }

      setDocumentCopierTask(documentCopierTask);

      // recheck task every 5 seconds
      setCheckTaskInterval(5000);
    } catch (e) {
      setStatus("idle");
      setErrorMessage((e as Error).message);
    }
  }, [document, status, targetClient]);

  const viewTargetDocument = React.useCallback(() => {
    window.location.href = window.location.href.replace(
      `/${dataset}/`,
      `/${copyTarget.dataset}/`
    );
  }, [document, copyTarget]);

  return (
    <Card tone="primary" padding={3} radius={3}>
      <Stack space={3}>
        <Flex
          align="center"
          justify="space-between"
          style={{ minHeight: "35px" }}
        >
          <Heading size={2}>{copyTarget.title}</Heading>
          {"checking" === status && (
            <Inline space={3}>
              <Text>Checking for document...</Text>
              <Spinner size={3} />
            </Inline>
          )}
          {"checking" !== status && (
            <Inline space={1}>
              <Button
                loading={"copying" === status}
                onClick={() => copyDocument()}
                tone={targetDocumentExists ? "critical" : "primary"}
                text={
                  targetDocumentExists
                    ? "Overwrite document"
                    : "Create document"
                }
              />
              {targetDocumentExists && (
                <Button
                  icon={EyeOpenIcon}
                  onClick={() => viewTargetDocument()}
                />
              )}
            </Inline>
          )}
        </Flex>
        {(documentCopierTask || errorMessage) && (
          <Box paddingBottom={2}>
            {documentCopierTask && (
              <Text>{documentCopierTask.statusMessage}</Text>
            )}
            {errorMessage && (
              <Text style={{ color: "red" }}>{errorMessage}</Text>
            )}
          </Box>
        )}
      </Stack>
    </Card>
  );
};
