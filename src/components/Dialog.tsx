import React from "react";
import sanityClient from "@sanity/client";
import { Stack, Heading } from "@sanity/ui";

import { DialogRow } from "./DialogRow";
import { resolveCurrentDataset } from "../helpers";

import type { DialogProps } from "../types";

export const Dialog: React.FC<DialogProps> = (props) => {
  const { dataset: defaultDataset, projectId, copyTargets } = props;

  const dataset = resolveCurrentDataset(defaultDataset, copyTargets);

  const baseClient = React.useMemo(
    () =>
      sanityClient({
        dataset,
        projectId,
        useCdn: false,
        withCredentials: true,
      }),
    [dataset, projectId]
  );

  return (
    <Stack space={3}>
      {copyTargets
        .filter((copyTarget) => copyTarget.dataset !== dataset)
        .map((copyTarget) => (
          <DialogRow
            {...props}
            dataset={dataset}
            copyTarget={copyTarget}
            baseClient={baseClient}
            key={copyTarget.dataset}
          />
        ))}
    </Stack>
  );
};
