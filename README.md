<img align="right" src="screenshot.png" />

# Sanity Copy Document Action

This package provides a [Sanity document action](https://www.sanity.io/docs/document-actions) which facilitates documents to be copied between datasets. This action only works with a connected DotSanity app.

- Provides a "Copy to..." action available from the bottom-right of all documents
- Renders a modal with a list of configured datasets, with buttons to copy the document
- Supports [Sanity Spaces](https://www.sanity.io/docs/spaces)
- Copies assets to the target dataset and relinks their references
- Merges references from the target document if they already exist (to prevent breaking assets which were manually fixed on the target side)
- Attempts to resolve references by handle (e.g. products, collections etc) in the target dataset when copying

## Playground example

An example of this action can found in the [DotDev Sanity Playground](https://dotdev-playground.sanity.studio).

- Login using the support@dotdev.com.au Google account
- Navigate to the [test document](https://dotdev-playground.sanity.studio/production/desk/movie;91ad6539-71fd-4017-a689-d762de2cb717)
- Make an update to the document (e.g. update the title) and publish it
- In the bottom-right, click the little arrow icon
- Click on the "Copy to..." action
- The resulting modal is rendered by this package

## Installation

First, install the package into your Sanity desk.

```bash
yarn add @dotdev/sanity-copy-document-action @sanity/client @sanity/icons @sanity/ui
# or
npm i @dotdev/sanity-copy-document-action @sanity/client @sanity/icons @sanity/ui --save
```

Add a new file to the root of your project called `resolveDocumentActions.js`. Update the contents of this file to match the configuration for your own Sanity desk.

```js
import defaultResolve from "part:@sanity/base/document-actions";

import { buildCopyDocumentAction } from "@dotdev/sanity-copy-document-action";

export default function resolveDocumentActions(props) {
  return [
    ...defaultResolve(props),
    buildCopyDocumentAction({
      projectId: "foobar",
      dataset: "production",
      copyTargets: [
        {
          title: "AU Website",
          dataset: "production",
        },
        {
          title: "NZ Website",
          dataset: "alternative",
        },
      ],
      reactifySanity: {
        endpointPrefix:
          "https://australia-southeast1-foobar.cloudfunctions.net/",
        shopName: "foobar.myshopify.com",
      },
    }),
  ];
}
```

Update the `parts` key in your `sanity.json` file to include your new document action resolver.

```js
"parts": [
  ...// other parts here
  {
    "implements": "part:@sanity/base/document-actions/resolver",
    "path": "resolveDocumentActions.js"
  }
],
```

If you are using [Sanity Spaces](https://www.sanity.io/docs/spaces), make sure your spaces are configured in the `sanity.json` file. This gives you a dropdown in the top-left, which allows the user to easily switch between datasets.

```js
...// the rest of sanity.json
"__experimental_spaces": [
  {
    "name": "production",
    "title": "AU Website",
    "default": true,
    "api": {
      "projectId": "foobar",
      "dataset": "production"
    }
  },
  {
    "name": "alternative",
    "title": "NZ Website",
    "default": true,
    "api": {
      "projectId": "foobar",
      "dataset": "alternative"
    }
  }
]
```

## Gotchas

- For UI simplicity reasons, the "Copy to..." action is only available for documents which have been published and do not have an active draft. This makes it obvious exactly which data is going to be copied to the target and that the target will be published.
